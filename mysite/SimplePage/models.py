from django.db import models
from tinymce.models import HTMLField


class SimplePage(models.Model):
	title = models.CharField(max_length = 150, default=None)
	summery = models.TextField(max_length=100, default=None)
	content = HTMLField(null=True)

	def __str__(self):
		return self.title