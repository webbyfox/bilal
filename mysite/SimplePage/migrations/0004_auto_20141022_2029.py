# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('SimplePage', '0003_auto_20141022_1751'),
    ]

    operations = [
        migrations.AlterField(
            model_name='simplepage',
            name='content',
            field=tinymce.models.HTMLField(null=True),
        ),
    ]
