# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('SimplePage', '0002_simplepage_summery'),
    ]

    operations = [
        migrations.AlterField(
            model_name='simplepage',
            name='summery',
            field=models.TextField(default=None, max_length=150),
        ),
    ]
