# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('SimplePage', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='simplepage',
            name='summery',
            field=models.CharField(default=None, max_length=150),
            preserve_default=True,
        ),
    ]
